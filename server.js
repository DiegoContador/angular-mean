// Get dependecias

let mongoose = require('mongoose');
const express = require('express');
const path = require('path');
const http = require('http');
const bodyparse = require('body-parser');

// Conexión a la BD MongoDb a traves de moongose

var dbURI = 'mongodb://localhost:27017/db_mean';
mongoose.connect(dbURI, { useNewUrlParser: true, useUnifiedTopology: true });

// Configuración de los eventos de la conexión moongose
mongoose.connection.on('connected', () => {

    console.log('Moongose default connection open to ' + dbURI);

});

mongoose.connection.on('error', () => {

    console.log('Moongose default connection error: ' + err);
})

mongoose.connection.on('disconnected', () => {

    console.log('Moongose default connection disconnected');

});

// Si el proceso 'Node' termina, se cierra la conexión moongose
process.on('SIGINT', () => {

    mongoose.connection.close(() => {

        console.log('Moongose default connection disconnected through app termination');

        process.exit(0);
    });

});

// ////////////////////////////////////////////////////////////////////

//Crear app express y config

const app = express();

// parsers for POST data
app.use(bodyparse.json());
app.use(bodyparse.urlencoded({ extended: false }));

// Config del directorio 'dist' como directorio estatico
// en este directorio tendremos los archivos obtenidos en el build de nuestra aplicación ANGULAR
app.use(express.static(path.join(__dirname, 'dist/MEAN')));

// Config de la rutas
app.get('/api', (req, res) => {

    res.send('La API funciona');

});

require('./server/routes/tarea')(app);

app.get('*', (req, res) => {

    res.sendFile(path.join(__dirname, 'dist/MEAN/index.html'));

});

// Config del puerto de escucha

const port = process.env.PORT || '3000';
app.set('port', port);

// Creamos el servidor http con la aplicación express y abrimos puerto

const server = http.createServer(app);
server.listen(port, () => console.log(`API running on localhost ${port}`));
