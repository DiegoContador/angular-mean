var Tarea = require('../models/tarea');


exports.list_all_tareas = ((req, res) => {

    Tarea.find({}, (err, tarea) => {
        if (err) res.send(err);
        res.json(tarea);

    });
});

exports.create_tarea = ((req, res) => {

    var new_tarea = new Tarea(req.body);
    new_tarea.save((err, tarea) => {

        if (err) res.send(err);
        res.json(tarea);

    });
})

exports.read_tarea = ((req, res) => {

    Tarea.findById(req.params.tareaId, (err, tarea) => {

        if (err) res.send(err);
        res.json(tarea);
    });

});

exports.update_tarea = ((req, res) => {

    Tarea.findByIdAndUpdate({ _id: req.params.tareaId }, req.body, { new: true }, (err, tarea) => {

        if (err) res.send(err);
        res.json(tarea);

    });
});

exports.delete_tarea = ((req, res) => {

    Tarea.deleteOne({ _id: req.params.tareaId }, (err, tarea) => {

        if (err) res.send(err);
        res.json({ message: 'Tarea eliminida correctamente' });
    });
})
